
public class NullObject implements Command {

	/* Null Object; prevents need for null check in invoker */
	
	@Override
	public void execute() {
		//Do Nothing
	}

}
