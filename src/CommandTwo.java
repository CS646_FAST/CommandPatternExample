
public class CommandTwo implements Command {

/* Command object; encapsulates a request as an object by binding together a set of actions on a specific receiver */
	
	ReceiverB receiverB;
	
	public CommandTwo(ReceiverB receiverB){
		/* This command object will perform actions from receiverB */
		this.receiverB = receiverB;
	}
	
	@Override
	public void execute() {
		/* Can contain as many actions from receiverB as needed to get a job done (in whatever order is needed) */
		receiverB.stepA();
		receiverB.stepB();
		receiverB.stepC();
	}

}
