
public class ReceiverB {

	/* One receiver; performs a specific action -> Can be 3rd party objects */
	
	public void stepA(){
		System.out.println("ReceiverB is performing stepA.");
	}
	
	public void stepB(){
		System.out.println("ReceiverB is performing stepB.");
	}
	
	public void stepC(){
		System.out.println("ReceiverB is performing stepC.");
	}
	
}
