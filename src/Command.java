
public interface Command {

	public void execute(); //command objects expose this method
	
}
