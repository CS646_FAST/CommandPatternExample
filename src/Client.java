
public class Client {

	/* Our client; makes use of the command pattern to do something */
	
	public static void main(String[] args) {
		
		//1: Create invoker
		Invoker invoker = new Invoker();
		
		//2: Create concrete products (receivers):
		ReceiverA receiverA = new ReceiverA();
		ReceiverB receiverB = new ReceiverB();
		
		//3: Create concrete command objects:
		CommandOne cmd1 = new CommandOne(receiverA);
		CommandTwo cmd2 = new CommandTwo(receiverB);
		
		Command[] macro = {cmd2, cmd1}; //create command array for macro:
		MacroCommand mCmd = new MacroCommand(macro); //create Macro Command (containing an array of commands)
		
		//4: Parameterize our invoker with command objects & perform actions:
		
		System.out.println(">----------Command1---------<");
		
		invoker.setCommand(cmd1);
		invoker.doAction();
		
		System.out.println(">----------Command2---------<");
		
		invoker.setCommand(cmd2); //load new command object into invoker slot
		invoker.doAction();
		
		System.out.println(">-----------Macro-----------<");
		
		invoker.setCommand(mCmd); //load macro command into invoker slot
		invoker.doAction();
		
		System.out.println(">------------END------------<");
		
		// More methods to perform actions with objects (receivers)
	}

}
